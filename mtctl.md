MTCTL(1) - General Commands Manual

# Name

**mtctl** - interact with
mtd(1)

# SYNOPSYS

**mtctl**
\[**-46**]
\[**-p**&nbsp;*port*]
\[**-h**&nbsp;*host*]
message

# DESCRIPTION

The
**mtctl**
utility is used to interact with a
mtd(1)
server. Please, keep in mind that
mtd(1)
uses UDP packets that are not reilable:
**mtctl**
cannot guarantee that the packet sent was delivered or not.

The options are as follows:

**-4**

> use IPv4 (default)

**-6**

> use IPv6

**-p** *port*

> the port. By default is 9999

**-h** *host*

> the host. By default is localhost

The defaults are modelled after
mtd(1).

# SEE ALSO

mtd(1)

# AUTHORS

Omar Polo &lt;[omar.polo@europecom.net](mailto:omar.polo@europecom.net)&gt;

OpenBSD 6.4 - January 22, 2019
