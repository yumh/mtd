/*
 * Copyright (c) 2019 Omar Polo <omar.polo@europecom.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <netinet/in.h>
#include <sys/socket.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <netdb.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

/* log and exit */
#define LE(code, level, msg)					\
	do {							\
		syslog(level, "%s: %s", msg, strerror(errno));	\
		exit(code);					\
	} while(0)

#define BUFLEN 65515

extern int optind;
extern char *optarg;

/* create a new list of string-pointer with size of 1 */
char **
array_new()
{
	return calloc(1, sizeof(char*));
}

void
array_destroy(char **arr, size_t len)
{
	size_t i;
	for (i = 0; i < len; ++i)
		free(arr[i]);
	free(arr);
}

int
array_append(char **arr, size_t len, char *m, size_t mlen)
{
	char *mdup, **t;
	size_t i, l;

	for (i = 0; i < len-1; ++i) {
		l = strlen(arr[i]);
		if (!strncmp(m, arr[i], MIN(l, mlen)))
			return 0;
	}


	if ((mdup = calloc(mlen + 1, 1)) == NULL)
		return -1;

	memcpy(mdup, m, mlen);

	if ((t = realloc(arr, len)) == NULL)
		return -1;

	arr = t;
	arr[len - 1] = mdup;

	return 1;
}

void
notify_send(char *nscript, char *title, char **arr, size_t len)
{
	size_t l, i;
	char *body;

	len -= 1;

	switch(fork()) {
	case -1:
		err(1, "fork");

	case 0:
		/* child */
		l = 0;
		for (i = 0; i < len; ++i)
			l += strlen(arr[i]) + 4; /* 1 for the newline, 4 for the " - " */

		if ((body = calloc(l, 1)) == NULL)
			return;

		l = 0;
		for (i = 0; i < len; ++i) {
			strcpy(&body[l], " - ");
			l += 3;

			strcpy(&body[l], arr[i]);
			l += strlen(arr[i]);

			strcpy(&body[l], "\n");
			l += 1;
		}

		execlp(nscript, nscript, title, body, NULL);
		err(1, "execlp");
		return;

	default:
		return;
	}
}

int
main(int argc, char **argv)
{
	int sock,i, r, s, delay, af, ch, error, foreground;
	char buf[BUFLEN], *prgname, **arr, *host, *port, *display;
	char *nscript, *title;
	size_t arrlen;
	struct pollfd fd;
	struct addrinfo hints, *res;

	signal(SIGCHLD, SIG_IGN);

	foreground = 0;
	prgname = argv[0];
	host = "localhost";
	port = "9999";
	af = AF_INET;
	delay = 5;
	nscript = "notify-send";
	title = "Mail";

	if ((display = getenv("DISPLAY")) == NULL)
		display = ":0";

	arrlen = 1;
	arr = array_new();

	while ((ch = getopt(argc, argv, "46fd:h:n:p:s:t:")) != -1) {
		switch (ch) {
		case '4':
			af = AF_INET;
			break;

		case '6':
			af = AF_INET6;
			break;

		case 'd':
			display = optarg;
			break;

		case 'f':
			foreground = 1;
			break;

		case 'h':
			host = optarg;
			break;

		case 'n':
			nscript = optarg;
			break;

		case 'p':
			port = optarg;
			break;

		case 's': {
			char *ep;
			long lval;

			errno = 0;
			lval = strtol(optarg, &ep, 10);
			if (optarg[0] == '\0' || *ep != '\0') {
				fprintf(stderr, "%s: not a number: ``%s''\n", prgname, optarg);
				return 1;
			}
			if ((errno == ERANGE && (lval == LONG_MAX || lval == LONG_MIN)) ||
			    (lval > INT_MAX || lval < INT_MIN)) {
				fprintf(stderr, "%s: value out of range: %s\n", prgname, optarg);
				return 1;
			}
			delay = lval;
			if (delay <= 0) {
				fprintf(stderr, "The delay should be greater or equal than 1 second.\n");
				return 1;
			}
			break;
		}

		case 't':
			title = optarg;
			break;

		default:
			/* fprintf(stderr, "Unknown option ``%c''\n", ch); */
			return 1;
		}
	}
	argc -= optind;
	argv += optind;
	if (argc != 0) {
		fprintf(stderr, "unknown argument: %s\n", argv[0]);
		return 1;
	}

	if (!foreground) {
		/* close all fds */
		for (i = getdtablesize(); i >= 0; i--)
			close(i);
		i = open("/dev/null", O_RDWR); /* open stdin */
		dup(i);		/* stdout */
		dup(i);		/* stderr */

		/* then fork */
		switch(fork()) {
		case -1:
			err(1, "fork");
			break;

		case 0:
			break;

		default:
			return 0;
		}
	}

	/* get my own progress group */
	if (setsid() == -1)
		LE(1, LOG_CRIT, "setsid");

	setenv("DISPLAY", display, 1);

	if ((sock = socket(af, SOCK_DGRAM, 0)) == -1)
		LE(1, LOG_CRIT, "socket error");

	if (fcntl(sock, F_SETFD, FD_CLOEXEC) == -1)
		LE(1, LOG_CRIT, "fcntl");

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = af;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;
	if ((error = getaddrinfo(host, port, &hints, &res)) != 0) {
		syslog(LOG_ERR, "getaddrinfo: %s", gai_strerror(error));
		return 1;
	}

	if (bind(sock, res->ai_addr, sizeof(struct sockaddr)) == -1)
		LE(1, LOG_ERR, "bind error");


	fd.fd = sock;
	fd.events = POLLIN;

#ifdef __OpenBSD__
	/*
	 * notify-send needs a LOT of pledges. I'm not sure how
	 * ``secure'' can be to have this daemon that can fork and
	 * its child can do anything, however...
	 */
	if (pledge("stdio proc exec", NULL) == -1)
		LE(1, LOG_ERR, "pledge err");
#endif

	s = 0;
	while (1) {
		r = poll(&fd, 1, delay * 1000);

		if (r == -1)
			LE(1, LOG_ERR, "poll");

		if (r == 0 && s) {
			notify_send(nscript, title, arr, arrlen);
			s = 0;
			array_destroy(arr, arrlen);
			arr = array_new();
			arrlen = 1;
		}

		if (r > 0) {
			s = 1;
			r = recv(sock, buf, BUFLEN, 0);

			/* ignore the error and continue */
			if (r == -1 && errno != EMSGSIZE) {
				syslog(LOG_ERR, "recv: %s", strerror(errno));
				continue;
			}

			arrlen += array_append(arr, arrlen, buf, r);
		}
	}

	close(sock);
	return 0;
}
