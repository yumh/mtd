MTD(1) - General Commands Manual

# Name

**mtd** - debounce notifications

# SYNOPSYS

**mtd**
\[**-46f**]
\[**-d**&nbsp;*display*]
\[**-h**&nbsp;*host*]
\[**-n**&nbsp;*script*]
\[**-p**&nbsp;*port*]
\[**-s**&nbsp;*delay*]
\[**-t**&nbsp;*title*]

# DESCRIPTION

The
**mtd**
utilty listen on a port and receives UDP packets containing text. If,
after some delay, no more packets are received, it'll spawn
notify-send with a notification that consist of an ASCII list of the
text received. If multiple copy of the same string are sent, only one
will be preserved.

The options are as follows:

**-4**

> use IPv4 (default)

**-6**

> use IPv6

**-f**

> run in the foreground.

**-d** *display*

> the value for the
> `DISPLAY`
> environment variable. By default is inherited by the environment, or
> :0 if it's not present.

**-h** *host*

> change the host on which the port will be binded. By defalut it's
> localhost.

**-n** *script*

> The script called to display the notification. By default is
> notify-send. The script should accept two parameters: the title and
> the text of the notification.

**-p** *port*

> set the port on which
> **mtd**
> will listen. By default it's 9999.

**-s** *delay*

> adjust the delay of the notification debounce in seconds. By default is 5.

**-t** *title*

> customize the title of the notification. The default value is "Mail".

# ENVIRONMENT

**mtd**
behaviour is affected by two environment variables:

`DISPLAY`

> gives the default value, if not overridden with
> **-d**, that is inherited by notify-send when it's executed.

`PATH`

> **mtd**
> uses
> execlp(3)
> to execute notify-send.

# SEE ALSO

mtctl(1)

# AUTHORS

Omar Polo &lt;[omar.polo@europecom.net](mailto:omar.polo@europecom.net)&gt;

# CAVEATS

Even if
**mtd**
is running in the foreground, the log will be sent to syslog instead of
being echoed on stderr.

OpenBSD 6.4 - January 22, 2019
