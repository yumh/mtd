/*
 * Copyright (c) 2019 Omar Polo <omar.polo@europecom.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <err.h>
#include <errno.h>
#include <limits.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

extern char *optarg;
extern int optind;

int
strthtons(const char *p)
{
	long port;
	char *ep;

	errno = 0;
	port = strtol(p, &ep, 10);
	if (p[0] == '\0' || *ep != '\0')
		return -1;
	if (errno == ERANGE && (port == LONG_MAX || port == LONG_MIN))
		return -1;

	return htons(port);
}

int
main(int argc, char **argv)
{
	int sock, ch, af, p, error;
	const char *prgname, *buf, *host, *port;
	struct addrinfo hints, *res;

	prgname = argv[0];

	port = "9999";
	af = AF_INET;
	host =  "localhost";

	while ((ch = getopt(argc, argv, "46p:h:")) != -1) {
		switch(ch) {
		case '4':
			af = AF_INET;
			break;

		case '6':
			af = AF_INET6;
			break;

		case 'p':
			port = optarg;
			break;

		case 'h':
			host = optarg;
			break;

		default:
			fprintf(stderr, "Unknown option ``%c''\n", ch);
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	if (argc != 1) {
		fprintf(stderr, "Missing message\n");
		return 1;
	}
	buf = argv[0];

	p = strthtons(port);
	if (port < 0) {
		fprintf(stderr, "Invalid port: %s\n", optarg);
		return 1;
	}

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = af;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;
	if ((error = getaddrinfo(host, port, &hints, &res)) != 0) {
		fprintf(stderr, "%s: getaddrinfo: %s\n", prgname, gai_strerror(error));
		return 1;
	}

	if ((sock = socket(af, SOCK_DGRAM, 0)) == -1)
		err(1, "socket error");

	if (sendto(sock, buf, strlen(buf), 0, res->ai_addr, sizeof(struct sockaddr)) == -1)
		err(1, "sendto");

	close(sock);
	freeaddrinfo(res);

	return 0;
}
