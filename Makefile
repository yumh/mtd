CC	?= cc
PREFIX	?= /usr/local

all: mtd mtctl

.PHONY: clean install manpages

mtd: mtd.c
	${CC} -g -O0 mtd.c -o mtd

mtctl: mtctl.c
	${CC} -g -O0 mtctl.c -o mtctl

clean:
	rm -f mtd mtctl

install: mtd mtctl
	cp mtd ${PREFIX}/bin
	cp mtctl ${PREFIX}/bin
	cp mtd.1 ${PREFIX}/man/man1/mtd.1
	cp mtctl.1 ${PREFIX}/man/man1/mtctl.1

manpages: mtd.md mtctl.md

.SUFFIXES: .1 .md
.1.md:
	mandoc -T markdown $< > $@
